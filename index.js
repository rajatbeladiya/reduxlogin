import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/App';
import { Provider } from "react-redux";
import { createStore } from "redux";
import reducer from "./src/Store/Reducer/Reducer";

const store = createStore(reducer);

const AppContainer = () => (
    <Provider store={store}>
        <App />
    </Provider>
);

AppRegistry.registerComponent('reduxLogin', () => AppContainer);
