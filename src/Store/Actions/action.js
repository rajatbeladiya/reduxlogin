export const counterIncrement = () => {
    return {
        type: "INCREMENT"
    };
};

export const counterDecrement = () => {
    return {
        type: "DECREMENT"
    };
};

export const counterClear = () => {
    return {
        type: "CLEAR"
    };
};

export const textUpdate = (text) => {
    return {
        type: "TEXT_UPDATE",
        payload: text
    };
};
