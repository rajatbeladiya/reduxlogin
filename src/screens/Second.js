import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'native-base';

export default class Second extends Component {
    render() {
        return (
            <View>
                <Text>Second world</Text>
                <Button onPress={() => this.props.navigation.navigate('Login')}><Text>Click</Text></Button>
            </View>
        )
    }
}