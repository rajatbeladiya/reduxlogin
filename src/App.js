import React, { Component } from 'react';
import { Root } from "native-base";
import { ApplicationStack } from './Router';



class App extends Component {
    render() {
        return (
            <Root>
                <ApplicationStack />
            </Root>
        )
    }
}
export default App
