import React from 'react'
import { StackNavigator, DrawerNavigator } from 'react-navigation'

import Login from './screens/Login';
import Second from './screens/Second';

export const ApplicationStack = StackNavigator({
    Login: {
        screen: Login,
        navigationOptions: ({ navigation }) => ({
            title: 'Login',
            header: null
        })
    },
    Second: {
        screen: Second,
        navigationOptions: ({ navigation }) => ({
            title: 'Second',
            header: null
        })
    },
},
    {
        initialRouteName: 'Login'
    })